package com.example.inspiringpeopleroom.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpeopleroom.R
import com.example.inspiringpeopleroom.interfaces.InspiringPersonInteractionListener
import com.example.inspiringpeopleroom.models.InspiringPerson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.inspiringperson_holder.view.*
import kotlinx.android.synthetic.main.inspiringperson_holder.view.tv_BirthDate
import kotlinx.android.synthetic.main.inspiringperson_holder.view.tv_DeathDate
import java.text.SimpleDateFormat

class InspiringPersonAdapter(private val listener: InspiringPersonInteractionListener) :
    RecyclerView.Adapter<InspiringPersonHolder>() {

    private val inspiringPeople = mutableListOf<InspiringPerson>()

    fun addAll(inspiringPersons: List<InspiringPerson>) {
        this.inspiringPeople.clear()
        this.inspiringPeople.addAll(inspiringPersons)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspiringPersonHolder =
        InspiringPersonHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.inspiringperson_holder, parent, false)
        )

    override fun onBindViewHolder(holder: InspiringPersonHolder, position: Int) =
        holder.bind(this.inspiringPeople[position], listener)

    override fun getItemCount() = this.inspiringPeople.size

}

class InspiringPersonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bind(inspiringPerson: InspiringPerson, listener: InspiringPersonInteractionListener) {
        itemView.tv_name.text = inspiringPerson.name
        itemView.tv_Description.text = inspiringPerson.description
        itemView.tv_BirthDate.text =
            "Birth date: ${inspiringPerson.birthDate}"
        if(inspiringPerson.deathDate != null){
            itemView.tv_DeathDate.text =
                "Death date: ${inspiringPerson.deathDate!!}"
        }
        else itemView.tv_DeathDate.visibility = View.GONE
        if(inspiringPerson.imageURI != String()){
            Picasso.get().load(inspiringPerson.imageURI).fit().placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image_error).into(itemView.iv_image)
        }
        else itemView.iv_image.setImageResource(R.drawable.ic_image)
        itemView.iv_image.setOnClickListener { listener.onImageClick(inspiringPerson.quotes) }
    }

}