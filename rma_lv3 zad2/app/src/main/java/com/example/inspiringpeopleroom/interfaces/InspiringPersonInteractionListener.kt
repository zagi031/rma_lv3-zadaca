package com.example.inspiringpeopleroom.interfaces

interface InspiringPersonInteractionListener {
    fun onImageClick(quotes: List<String>)
}