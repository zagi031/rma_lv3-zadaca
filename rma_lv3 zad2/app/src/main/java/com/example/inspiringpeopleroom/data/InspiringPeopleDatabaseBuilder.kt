package com.example.inspiringpeopleroom.data

import androidx.room.Room
import com.example.inspiringpeopleroom.InspiringPeople

object InspiringPeopleDatabaseBuilder {
    private var instance: InspiringPeopleDatabase? = null

    fun getInstance(): InspiringPeopleDatabase {
        synchronized(InspiringPeopleDatabase::class) {
            if (instance == null) {
                instance = buildDatabase()
            }
        }
        return instance!!
    }

    private fun buildDatabase(): InspiringPeopleDatabase {
        return Room.databaseBuilder(
            InspiringPeople.context,
            InspiringPeopleDatabase::class.java,
            InspiringPeopleDatabase.DBNAME
        ).allowMainThreadQueries().build()
    }
}