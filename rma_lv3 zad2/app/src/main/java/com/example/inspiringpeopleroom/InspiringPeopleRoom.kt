package com.example.inspiringpeopleroom

import android.app.Application

class InspiringPeople: Application() {
    companion object {
        lateinit var context: InspiringPeople
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }

}