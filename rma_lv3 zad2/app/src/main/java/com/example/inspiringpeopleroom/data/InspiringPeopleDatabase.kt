package com.example.inspiringpeopleroom.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.inspiringpeopleroom.models.Converters
import com.example.inspiringpeopleroom.models.InspiringPerson

@Database(entities = [InspiringPerson::class], version = 1)
@TypeConverters(Converters::class)
abstract class InspiringPeopleDatabase : RoomDatabase() {
    abstract fun inspiringPeopleDao(): InspiringPeopleDao

    companion object {
        const val DBNAME = "inspiringPeopleDb"
    }
}