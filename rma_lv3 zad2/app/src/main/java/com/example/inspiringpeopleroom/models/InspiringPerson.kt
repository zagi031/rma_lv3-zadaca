package com.example.inspiringpeopleroom.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter

@Entity(tableName = "inspiringPeople")
data class InspiringPerson(
    @PrimaryKey(autoGenerate = true) val id: Long,
    var name: String = "",
    var description: String = "",
    var birthDate: String = "",
    var imageURI: String = "",
    var quotes: List<String> = listOf()
) {

    var deathDate: String? = null

    constructor(
        name: String,
        description: String,
        birthDate: String,
        deathDate: String,
        imageURI: String,
        quotes: List<String>
    ) : this(0, name, description, birthDate, imageURI, quotes) {
        this.deathDate = deathDate
    }
}

class Converters {
    @TypeConverter
    fun fromListofStringToString(value: List<String>): String? {
        val delimiter = "."
        var result = ""
        value.forEach {
            result += "$it$delimiter"
        }
        return result
    }

    @TypeConverter
    fun fromStringToListOfString(value: String): List<String>? {
        val delimiter = "."
        val result = mutableListOf<String>()
        value.split(delimiter).filter { it.isNotBlank() }.forEach {
            result.add(it)
        }
        return result
    }
}