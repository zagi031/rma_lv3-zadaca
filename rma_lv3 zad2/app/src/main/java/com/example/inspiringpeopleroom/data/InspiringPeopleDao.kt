package com.example.inspiringpeopleroom.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.inspiringpeopleroom.models.InspiringPerson

@Dao
interface InspiringPeopleDao {
    @Query("SELECT * FROM inspiringPeople")
    fun getInspiringPeople(): LiveData<List<InspiringPerson>>

    @Insert
    fun saveInspiringPerson(person: InspiringPerson)
}