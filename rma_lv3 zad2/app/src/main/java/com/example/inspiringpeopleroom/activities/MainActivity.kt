package com.example.inspiringpeopleroom.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpeopleroom.adapters.InspiringPersonAdapter
import com.example.inspiringpeopleroom.interfaces.InspiringPersonInteractionListener
import com.example.inspiringpeopleroom.R
import com.example.inspiringpeopleroom.data.InspiringPeopleDao
import com.example.inspiringpeopleroom.data.InspiringPeopleDatabaseBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), InspiringPersonInteractionListener {
    private val adapter = InspiringPersonAdapter(this)
    private val inspiringPeopleRepository: InspiringPeopleDao =
        InspiringPeopleDatabaseBuilder.getInstance().inspiringPeopleDao()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerview()
        setupListeners()
    }

    private fun setupRecyclerview() {
        rc_inspiringPeoples.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rc_inspiringPeoples.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        rc_inspiringPeoples.adapter = adapter
    }

    private fun setupListeners() {
        inspiringPeopleRepository.getInspiringPeople().observe(this) {
            adapter.addAll(it)
        }

        btn_ShowAddInspiringPersonActivity.setOnClickListener {
            startActivity(Intent(this, AddInspiringPersonActivity::class.java))
        }
    }

    override fun onImageClick(quotes: List<String>) =
        Toast.makeText(this, quotes[(quotes.indices).random()], Toast.LENGTH_SHORT).show()
}