package com.example.inspiringpeopleroom.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.inspiringpeopleroom.models.InspiringPerson
import com.example.inspiringpeopleroom.R
import com.example.inspiringpeopleroom.data.InspiringPeopleDao
import com.example.inspiringpeopleroom.data.InspiringPeopleDatabaseBuilder
import kotlinx.android.synthetic.main.activity_add_inspiring_person.*
import java.util.*

class AddInspiringPersonActivity : AppCompatActivity() {

    private val inspiringPeopleRepository: InspiringPeopleDao =
        InspiringPeopleDatabaseBuilder.getInstance().inspiringPeopleDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_inspiring_person)

        rg_isDead.setOnCheckedChangeListener { group, checkedId ->
            if (rbtn_isDead.isChecked) {
                tv_DeathDate.visibility = View.VISIBLE
                dp_DeathDate.visibility = View.VISIBLE
            } else {
                tv_DeathDate.visibility = View.GONE
                dp_DeathDate.visibility = View.GONE
            }
        }

        btn_AddInspiringPerson.setOnClickListener {
            val name = et_Name.text.toString().trim()
            val description = et_Description.text.toString().trim()
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.YEAR, dp_BirthDate.year)
            calendar.set(Calendar.MONTH, dp_BirthDate.month)
            calendar.set(Calendar.DAY_OF_MONTH, dp_BirthDate.dayOfMonth)
            val birthDate = calendar.time
            val imageURL = et_imageURL.text.toString()
            val quotesText = et_quote.text.toString().trim()
            var quotes = listOf<String>()
            if (quotesText != String()) {
                val delimiter = "."
                quotes = quotesText.split(delimiter).filter { it.isNotBlank() }
            }


            if (name != String() && description != String() && quotes.isNotEmpty()) {    //person image has placeholder in case of empty imageURL
                if (rbtn_isDead.isChecked) {
                    calendar.set(Calendar.YEAR, dp_DeathDate.year)
                    calendar.set(Calendar.MONTH, dp_DeathDate.month)
                    calendar.set(Calendar.DAY_OF_MONTH, dp_DeathDate.dayOfMonth)
                    val deathDate = calendar.time
                    if (birthDate < deathDate) {
                        inspiringPeopleRepository.saveInspiringPerson(
                            InspiringPerson(
                                name,
                                description,
                                birthDate.toString(),
                                deathDate.toString(),
                                imageURL,
                                quotes
                            )
                        )
                        this.finish()
                    } else Toast.makeText(this, "Check birth and death dates.", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    inspiringPeopleRepository.saveInspiringPerson(
                        InspiringPerson(
                            0,
                            name,
                            description,
                            birthDate.toString(),
                            imageURL,
                            quotes
                        )
                    )
                    this.finish()
                }
            } else Toast.makeText(this, "Some of the fields are empty.", Toast.LENGTH_SHORT).show()
        }
    }
}