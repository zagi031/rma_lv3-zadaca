package com.example.birdobserver

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.birdobserver.TextViewAppearanceHelper.getTextViewAppearance
import com.example.birdobserver.databinding.ActivityMainBinding
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var birdCounter = 0
    private var tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.NONE_KEY)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpListeners()
    }

    override fun onResume() {
        super.onResume()
        birdCounter = SharedPrefsManager.getBirdCount()
        tvBirdCounterAppearance = SharedPrefsManager.getBirdCounterAppearance()
        binding.tvCountedBirds.text = this.birdCounter.toString()
        setTvBirdCounterAppearance()
    }

    override fun onPause() {
        super.onPause()
        SharedPrefsManager.saveBirdCount(birdCounter)
        SharedPrefsManager.saveBirdCounterAppearance(tvBirdCounterAppearance)
    }

    private fun setUpListeners() {
        binding.btnBlackBird.setOnClickListener {
            this.tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.BLACKBIRD_KEY)
            incrementBirdCounter()
            setTvBirdCounterAppearance()
        }
        binding.btnWhiteBird.setOnClickListener {
            this.tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.WHITEBIRD_KEY)
            incrementBirdCounter()
            setTvBirdCounterAppearance()
        }
        binding.btnGrayBird.setOnClickListener {
            this.tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.GRAYBIRD_KEY)
            incrementBirdCounter()
            setTvBirdCounterAppearance()
        }
        binding.btnBrownBird.setOnClickListener {
            this.tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.BROWNBIRD_KEY)
            incrementBirdCounter()
            setTvBirdCounterAppearance()
        }
        binding.btnReset.setOnClickListener {
            resetBirdCounter()
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun incrementBirdCounter() {
        this.birdCounter++
        this.binding.tvCountedBirds.text = birdCounter.toString()
    }

    private fun setTvBirdCounterAppearance() {
        this.binding.tvCountedBirds.background = ContextCompat.getDrawable(this, tvBirdCounterAppearance.backgroundResID)
        this.binding.tvCountedBirds.setTextColor(ContextCompat.getColor(this, tvBirdCounterAppearance.textColorResID))
    }

    private fun resetBirdCounter() {
        this.birdCounter = 0
        this.binding.tvCountedBirds.text = birdCounter.toString()
        this.tvBirdCounterAppearance = getTextViewAppearance(TextViewAppearanceHelper.NONE_KEY)
        setTvBirdCounterAppearance()
    }
}