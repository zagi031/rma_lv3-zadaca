package com.example.birdobserver

data class TextViewAppearance(val backgroundResID: Int, val textColorResID: Int)
