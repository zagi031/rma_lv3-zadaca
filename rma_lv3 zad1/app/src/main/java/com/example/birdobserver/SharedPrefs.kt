package com.example.birdobserver

import com.google.gson.Gson

interface SharedPrefs {
    fun saveBirdCount(birdCount: Int)
    fun getBirdCount(): Int
    fun saveBirdCounterAppearance(appearance: TextViewAppearance)
    fun getBirdCounterAppearance(): TextViewAppearance
}